default:
    image: registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/alpine-sdk:stable

stages:
    - build
    - package
    - test
    - deploy

variables:
    PAGES_BRANCH: "main"
    GIT_DEPTH: 1
    ALPINE_VERSION: "3.21"
    OS_RELEASE_HASH: "209e837b749eb948a73c9f059f14b95e79ee6226148e591c3baf5ddb5dea7083"
    keyname: "lavcorps@protonmail.com.rsa"

.x86_64:
    tags:
        - amd64
    image:
        name: registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/alpine-sdk:stable
        docker:
            platform: linux/amd64

.aarch64:
    tags:
        - arm64
    image:
        name: registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/alpine-sdk:stable
        docker:
            platform: linux/arm64

.prebuild: &prebuild
    - |
        printf "\n${COLOR_F_MAGENTA_D}Placing credentials...${COLOR_F_DEFAULT}\n"
        mkdir -p ~/.abuild
        echo "PACKAGER_PRIVKEY=$BUILD_KEY" >> ~/.abuild/abuild.conf
        cp $BUILD_KEY_PUB $BUILD_KEY.pub
        printf "\n${COLOR_F_MAGENTA_D}Verifying ALPINE_VERSION...${COLOR_F_DEFAULT}\n"
        if [ "$(sha256sum /etc/os-release)" != "${OS_RELEASE_HASH}  /etc/os-release" ]; then
            printf "\n${COLOR_F_RED_D}FATAL    : ${COLOR_F_YELLOW_D}Running an unexpected version of Alpine Linux!${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_RED_D}EXPECTED : ${COLOR_F_MAGENTA_D}${OS_RELEASE_HASH}  /etc/os-release\n"
            printf "\n${COLOR_F_RED_D}ACTUAL   : ${COLOR_F_MAGENTA_D}$(sha256sum /etc/os-release)\n"
            printf "\n${COLOR_F_YELLOW_D}Steps to resolve this error:${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_YELLOW_D}1. Create a new branch${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_YELLOW_D}2. Update OS_RELEASE_HASH in .gitlab-ci.yml${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_YELLOW_D}3. Verify correct functionality${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_YELLOW_D}4. Merge back into main${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_CYAN_D}Contents of /etc/os-release:${COLOR_F_DEFAULT}\n"
            cat /etc/os-release
            exit 39
        fi

.ghcup-handler: &ghcup-handler
    - |
        if [ "$CI_JOB_STATUS" == "failed" ]; then
            printf "\n\e[0Ksection_start:`date +%s`:handler\r\e[0K${COLOR_F_RED_D}Job failure detected...${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_YELLOW_D}The unfortunate thing about this state is that we don't really know what exactly caused the job to fail.${COLOR_F_DEFAULT}\n"
            printf "\n${COLOR_F_YELLOW_D}We can take a few educated guesses, though.${COLOR_F_DEFAULT}\n"
            if [ -d "/home/user/.ghcup/logs" ]; then
                printf "\n${COLOR_F_MAGENTA_D}Displaying ghcup logs...${COLOR_F_DEFAULT}\n"
                cat /home/user/.ghcup/logs/*
            fi
            printf "\n\e[0Ksection_end:`date +%s`:handler\r\e[0K\n"
        fi

build dhall x86_64:
    extends: .x86_64
    stage: build
    variables:
        GHC_VERSION: 8.10.7
        STACK_VERSION: 2.9.3
        srcdir: $CI_PROJECT_DIR/artifacts/dhall-build
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${srcdir}"
            cd "${CI_PROJECT_DIR}/dhall"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:deps\r\e[0K${COLOR_F_MAGENTA_D}Installing Dependencies...${COLOR_F_DEFAULT}\n"
            abuild deps
            printf "\n\e[0Ksection_end:`date +%s`:deps\r\e[0K\n"
            printf "\n${COLOR_F_MAGENTA_D}Building...${COLOR_F_DEFAULT}\n"
            export PATH=$PATH:~/.local/bin:~/.ghcup/bin
            abuild prepare
            abuild -r build
            printf "\n\e[0Ksection_start:`date +%s`:undeps\r\e[0K${COLOR_F_MAGENTA_D}Uninstalling Dependencies...${COLOR_F_DEFAULT}\n"
            abuild undeps
            printf "\n\e[0Ksection_end:`date +%s`:undeps\r\e[0K\n"
    after_script:
        - *ghcup-handler
    artifacts:
        paths:
            - artifacts/dhall-build/bin
        expire_in: 1 hour
    rules:
        -
            if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
            when: manual
        -
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    retry: 2

build mdbook x86_64:
    extends: .x86_64
    stage: build
    variables:
        host: x86_64-unknown-linux-musl
        toolchain: stable
        srcdir: $CI_PROJECT_DIR/artifacts/mdbook-build
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${srcdir}"
            cd "${CI_PROJECT_DIR}/mdbook"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:deps\r\e[0K${COLOR_F_MAGENTA_D}Installing Dependencies...${COLOR_F_DEFAULT}\n"
            abuild deps
            printf "\n\e[0Ksection_end:`date +%s`:deps\r\e[0K\n"
            printf "\n${COLOR_F_MAGENTA_D}Building...${COLOR_F_DEFAULT}\n"
            abuild -r build
            printf "\n\e[0Ksection_start:`date +%s`:undeps\r\e[0K${COLOR_F_MAGENTA_D}Uninstalling Dependencies...${COLOR_F_DEFAULT}\n"
            abuild undeps
            printf "\n\e[0Ksection_end:`date +%s`:undeps\r\e[0K\n"
    artifacts:
        paths:
            - artifacts/mdbook-build/target/x86_64-unknown-linux-musl/release/mdbook
        expire_in: 1 hour
    rules:
        -
            if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
            when: manual
        -
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    retry: 2

build mdbook aarch64:
    extends: .aarch64
    stage: build
    variables:
        host: aarch64-unknown-linux-musl
        toolchain: stable
        srcdir: $CI_PROJECT_DIR/artifacts/mdbook-build
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${srcdir}"
            cd "${CI_PROJECT_DIR}/mdbook"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:deps\r\e[0K${COLOR_F_MAGENTA_D}Installing Dependencies...${COLOR_F_DEFAULT}\n"
            abuild deps
            printf "\n\e[0Ksection_end:`date +%s`:deps\r\e[0K\n"
            printf "\n${COLOR_F_MAGENTA_D}Building...${COLOR_F_DEFAULT}\n"
            abuild -r build
            printf "\n\e[0Ksection_start:`date +%s`:undeps\r\e[0K${COLOR_F_MAGENTA_D}Uninstalling Dependencies...${COLOR_F_DEFAULT}\n"
            abuild undeps
            printf "\n\e[0Ksection_end:`date +%s`:undeps\r\e[0K\n"
    artifacts:
        paths:
            - artifacts/mdbook-build/target/aarch64-unknown-linux-musl/release/mdbook
        expire_in: 1 hour
    rules:
        -
            if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
            when: manual
        -
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    retry: 2

package dhall x86_64:
    extends: .x86_64
    needs: ["build dhall x86_64"]
    stage: package
    variables:
        pkgdir: $CI_PROJECT_DIR/artifacts/packages/x86_64/dhall-package
        srcdir: $CI_PROJECT_DIR/artifacts/dhall-build
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${pkgdir}"
            cd "${CI_PROJECT_DIR}/dhall"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:pack\r\e[0K${COLOR_F_MAGENTA_D}Packaging...${COLOR_F_DEFAULT}\n"
            abuild -r -P "${pkgdir}" rootpkg
            printf "\n\e[0Ksection_end:`date +%s`:pack\r\e[0K\n"
    artifacts:
        paths:
            - artifacts/packages/x86_64/dhall-package/
        expire_in: 1 hour
    retry: 2

package lavcorps-keys noarch:
    needs: []
    stage: package
    variables:
        pkgdir: $CI_PROJECT_DIR/artifacts/packages/noarch/key-package
        srcdir: $CI_PROJECT_DIR/lavcorps-keys
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${pkgdir}"
            cd lavcorps-keys
            cp "${BUILD_KEY_PUB}" "${srcdir}/${keyname}.pub"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:verify\r\e[0K${COLOR_F_MAGENTA_D}Verifying...${COLOR_F_DEFAULT}\n"
            mkdir -p "${srcdir}"
            abuild verify
            printf "\n\e[0Ksection_end:`date +%s`:verify\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:pack\r\e[0K${COLOR_F_MAGENTA_D}Packaging...${COLOR_F_DEFAULT}\n"
            abuild -r -P "${pkgdir}" rootpkg
            printf "\n\e[0Ksection_end:`date +%s`:pack\r\e[0K\n"
    artifacts:
        paths:
            - artifacts/packages/noarch/key-package/
        expire_in: 1 hour
    retry: 2

package mdbook x86_64:
    extends: .x86_64
    needs: ["build mdbook x86_64"]
    stage: package
    variables:
        host: x86_64-unknown-linux-musl
        srcdir: $CI_PROJECT_DIR/artifacts/mdbook-build
        pkgdir: $CI_PROJECT_DIR/artifacts/packages/x86_64/mdbook-package
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${pkgdir}"
            cd "${CI_PROJECT_DIR}/mdbook"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:pack\r\e[0K${COLOR_F_MAGENTA_D}Packaging...${COLOR_F_DEFAULT}\n"
            abuild -r -P "${pkgdir}" rootpkg
            printf "\n\e[0Ksection_end:`date +%s`:pack\r\e[0K\n"
    artifacts:
        paths:
            - artifacts/packages/x86_64/mdbook-package/
        expire_in: 1 hour
    retry: 2

package mdbook aarch64:
    extends: .aarch64
    needs: ["build mdbook aarch64"]
    stage: package
    variables:
        host: aarch64-unknown-linux-musl
        srcdir: $CI_PROJECT_DIR/artifacts/mdbook-build
        pkgdir: $CI_PROJECT_DIR/artifacts/packages/aarch64/mdbook-package
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${pkgdir}"
            cd "${CI_PROJECT_DIR}/mdbook"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:pack\r\e[0K${COLOR_F_MAGENTA_D}Packaging...${COLOR_F_DEFAULT}\n"
            abuild -r -P "${pkgdir}" rootpkg
            printf "\n\e[0Ksection_end:`date +%s`:pack\r\e[0K\n"
    artifacts:
        paths:
            - artifacts/packages/aarch64/mdbook-package/
        expire_in: 1 hour
    retry: 2

test dhall x86_64:
    extends: .x86_64
    needs: []
    stage: test
    variables:
        GHC_VERSION: 8.10.7
        STACK_VERSION: 2.9.3
        srcdir: $CI_PROJECT_DIR/artifacts/dhall-build
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${srcdir}"
            cd "${CI_PROJECT_DIR}/dhall"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:deps\r\e[0K${COLOR_F_MAGENTA_D}Installing Dependencies...${COLOR_F_DEFAULT}\n"
            abuild deps
            printf "\n\e[0Ksection_end:`date +%s`:deps\r\e[0K\n"
            printf "\n${COLOR_F_MAGENTA_D}Testing...${COLOR_F_DEFAULT}\n"
            export PATH=$PATH:~/.local/bin:~/.ghcup/bin
            abuild prepare
            abuild -r check
            printf "\n\e[0Ksection_start:`date +%s`:undeps\r\e[0K${COLOR_F_MAGENTA_D}Uninstalling Dependencies...${COLOR_F_DEFAULT}\n"
            abuild undeps
            printf "\n\e[0Ksection_end:`date +%s`:undeps\r\e[0K\n"
    after_script:
        - *ghcup-handler
    rules:
        -
            if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
            when: manual
        -
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    retry: 2

test mdbook x86_64:
    extends: .x86_64
    needs: []
    stage: test
    variables:
        host: x86_64-unknown-linux-musl
        toolchain: stable
        srcdir: $CI_PROJECT_DIR/artifacts/mdbook-build
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${srcdir}"
            cd "${CI_PROJECT_DIR}/mdbook"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:deps\r\e[0K${COLOR_F_MAGENTA_D}Installing Dependencies...${COLOR_F_DEFAULT}\n"
            abuild deps
            printf "\n\e[0Ksection_end:`date +%s`:deps\r\e[0K\n"
            printf "\n${COLOR_F_MAGENTA_D}Testing...${COLOR_F_DEFAULT}\n"
            abuild -r check
            printf "\n\e[0Ksection_start:`date +%s`:undeps\r\e[0K${COLOR_F_MAGENTA_D}Uninstalling Dependencies...${COLOR_F_DEFAULT}\n"
            abuild undeps
            printf "\n\e[0Ksection_end:`date +%s`:undeps\r\e[0K\n"
    rules:
        -
            if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
            when: manual
        -
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    retry: 2

test mdbook aarch64:
    extends: .aarch64
    needs: []
    stage: test
    variables:
        host: aarch64-unknown-linux-musl
        toolchain: stable
        srcdir: $CI_PROJECT_DIR/artifacts/mdbook-build
    before_script:
        - *prebuild
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p "${srcdir}"
            cd "${CI_PROJECT_DIR}/mdbook"
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:fetch\r\e[0K${COLOR_F_MAGENTA_D}Fetching & Verifying...${COLOR_F_DEFAULT}\n"
            abuild fetch verify
            printf "\n\e[0Ksection_end:`date +%s`:fetch\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:unpack\r\e[0K${COLOR_F_MAGENTA_D}Unpacking...${COLOR_F_DEFAULT}\n"
            abuild unpack
            printf "\n\e[0Ksection_end:`date +%s`:unpack\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:deps\r\e[0K${COLOR_F_MAGENTA_D}Installing Dependencies...${COLOR_F_DEFAULT}\n"
            abuild deps
            printf "\n\e[0Ksection_end:`date +%s`:deps\r\e[0K\n"
            printf "\n${COLOR_F_MAGENTA_D}Testing...${COLOR_F_DEFAULT}\n"
            abuild -r check
            printf "\n\e[0Ksection_start:`date +%s`:undeps\r\e[0K${COLOR_F_MAGENTA_D}Uninstalling Dependencies...${COLOR_F_DEFAULT}\n"
            abuild undeps
            printf "\n\e[0Ksection_end:`date +%s`:undeps\r\e[0K\n"
    rules:
        -
            if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
            when: manual
        -
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    retry: 2

compile repo:
    needs: ["package dhall x86_64","package lavcorps-keys noarch","package mdbook x86_64","package mdbook aarch64"]
    stage: deploy
    before_script:
        - *prebuild
    script:
        - |
            printf "\n${COLOR_F_MAGENTA_D}Compiling Repository...${COLOR_F_DEFAULT}\n"
            mkdir -p "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/x86_64"
            mkdir -p "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/aarch64"
            mkdir -p "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/noarch"
            find "${CI_PROJECT_DIR}/artifacts/packages/x86_64" -iname "*.apk"
            find "${CI_PROJECT_DIR}/artifacts/packages/x86_64" -iname "*.apk" -exec cp "{}" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/x86_64" \;
            find "${CI_PROJECT_DIR}/artifacts/packages/aarch64" -iname "*.apk"
            find "${CI_PROJECT_DIR}/artifacts/packages/aarch64" -iname "*.apk" -exec cp "{}" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/aarch64" \;
            find "${CI_PROJECT_DIR}/artifacts/packages/noarch" -iname "*.apk"
            find "${CI_PROJECT_DIR}/artifacts/packages/noarch" -iname "*.apk" -exec cp "{}" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/noarch" \;
            cp "${BUILD_KEY_PUB}" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/${keyname}.pub"
            printf "\n${COLOR_F_MAGENTA_D}Building x86_64 index......${COLOR_F_DEFAULT}\n"
            cd "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/x86_64"
            find "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/x86_64" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/noarch" -iname "*.apk"
            apk --allow-untrusted index -d "v${ALPINE_VERSION}-${CI_COMMIT_SHORT_SHA}" -vU -o APKINDEX.tar.gz $(find "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/x86_64" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/noarch" -iname "*.apk")
            printf "\n${COLOR_F_MAGENTA_D}Signing x86_64 index...${COLOR_F_DEFAULT}\n"
            abuild-sign -k "${BUILD_KEY}" -p "${keyname}.pub" APKINDEX.tar.gz
            printf "\n${COLOR_F_MAGENTA_D}Building aarch64 index......${COLOR_F_DEFAULT}\n"
            cd "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/aarch64"
            find "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/aarch64" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/noarch" -iname "*.apk"
            apk --allow-untrusted index -d "v${ALPINE_VERSION}-${CI_COMMIT_SHORT_SHA}" -vU -o APKINDEX.tar.gz $(find "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/aarch64" "${CI_PROJECT_DIR}/artifacts/alpine-repo/v${ALPINE_VERSION}/noarch" -iname "*.apk")
            printf "\n${COLOR_F_MAGENTA_D}Signing aarch64 index...${COLOR_F_DEFAULT}\n"
            abuild-sign -k "${BUILD_KEY}" -p "${keyname}.pub" APKINDEX.tar.gz
    artifacts:
        paths:
            - artifacts/alpine-repo/
        expire_in: 1 hour
    retry: 2

s3-upload:
    needs: ["compile repo", "test dhall x86_64", "test mdbook x86_64", "test mdbook aarch64"]
    stage: deploy
    script:
        - |
            printf "\n\e[0Ksection_start:`date +%s`:prep\r\e[0K${COLOR_F_MAGENTA_D}Preparing...${COLOR_F_DEFAULT}\n"
            mkdir -p ~/.config/rclone
            printf "[swfs]\n" >> ~/.config/rclone/rclone.conf
            printf "type = s3\n" >> ~/.config/rclone/rclone.conf
            printf "provider = Other\n" >> ~/.config/rclone/rclone.conf
            printf "access_key_id = ${AWS_ACCESS_KEY_ID}\n" >> ~/.config/rclone/rclone.conf
            printf "secret_access_key = ${AWS_SECRET_ACCESS_KEY}\n" >> ~/.config/rclone/rclone.conf
            printf "endpoint = https://s3.lavcorps.xyz\n" >> ~/.config/rclone/rclone.conf
            printf "upload_cutoff = 50Mi\n" >> ~/.config/rclone/rclone.conf
            printf "chunk_size = 50Mi\n" >> ~/.config/rclone/rclone.conf
            printf "force_path_style = true\n" >> ~/.config/rclone/rclone.conf
            sudo apk add rclone
            mv artifacts/alpine-repo staging
            printf "\n\e[0Ksection_end:`date +%s`:prep\r\e[0K\n"
            printf "\n\e[0Ksection_start:`date +%s`:deploy\r\e[0K${COLOR_F_MAGENTA_D}Deploying...${COLOR_F_DEFAULT}\n"
            rclone  --log-level INFO sync  --checksum --fast-list ${CI_PROJECT_DIR}/staging/v${ALPINE_VERSION} swfs:/alpine/v${ALPINE_VERSION}
            printf "\n\e[0Ksection_end:`date +%s`:deploy\r\e[0K\n"
    rules:
        -
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    retry: 2

